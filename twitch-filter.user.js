// ==UserScript==
// @name         twitch-filter
// @namespace    https://www.twitch.tv/
// @version      0.1
// @description  Hide the games you don't like
// @author       pastalian
// @match        https://www.twitch.tv/directory
// @match        https://www.twitch.tv/directory/all
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const hideList = [
        'Among Us',
        'Apex Legends',
        'Call of Duty: Warzone',
        'Counter-Strike: Global Offensive',
        'Chess',
        'Dead by Daylight',
        'Dota 2',
        'Escape From Tarkov',
        'FIFA 21',
        'Fortnite',
        'Grand Theft Auto V',
        'Hearthstone',
        'League of Legends',
        'Minecraft',
        'Overwatch',
        'Rust',
        'Slots',
        'Sports',
        'VALORANT',
        'World of Warcraft',
    ];

    const hideGames = function() {
        // Categories
        const games_c = document.querySelectorAll('h3.tw-ellipsis.tw-font-size-5.tw-line-height-body');
        for (const game of games_c) {
            if (hideList.includes(game.title)) {
                game.closest('div[data-target="directory-page__card-container"]').parentNode.remove();
            }
        }

        // Live Channels
        const games_l = document.querySelectorAll('a[data-test-selector="GameLink"]');
        for (const game of games_l) {
            if (hideList.includes(game.text)) {
                game.closest('div[data-target="directory-game__card_container"]').parentNode.remove();
            }
        }
    }

    const targetNode = document.getElementById('root');
    const config = { childList: true, subtree: true };

    const observer = new MutationObserver(hideGames);
    observer.observe(targetNode, config);
})();
