// ==UserScript==
// @name        Monospace Gmail
// @namespace   https://github.com/pastalian
// @match       https://mail.google.com/*
// @grant       GM_addStyle
// @version     1.0
// @author      pastalian
// @description Use monospace in Gmail
// ==/UserScript==

let css = `
.a3s, .Am {
  font-family: monospace !important;
}
`

GM_addStyle(css);
